package gg.projects.levertonchallange.repository;

import gg.projects.levertonchallange.entity.Url;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author gohar.gasparyan
 */
@Repository
public interface UrlRepository extends CrudRepository<Url, Long> {

    Optional<Url> findByOriginalUrl(String originalUrl);

    Optional<Url> findByShortUrl(String shortUrl);

}
