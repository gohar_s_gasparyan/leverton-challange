package gg.projects.levertonchallange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LevertonChallangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(LevertonChallangeApplication.class, args);
	}
}
